package com.ioof.robotchallenge;

import com.ioof.robotchallenge.command.Facing;

/**
 * This class represents a <code>Pose</code> which is a 2D representation of x,y position and direction.
 *
 * @author Craig Tinkler (craig@tink-tech.com)
 * 
 */
public class Pose {
	
	/**
	 * The X,Y position within a 2D Grid.
	 */
	private Point position;
	
	/**
	 * The direction that the Pose represents. This can be North, South, East or West facing in relation to a Grid
	 */
	private Facing direction;
	
	/**
	 * Initialises a new instance of <code>Pose</code> with the 
	 * specified position and direction of facing.
	 *
	 * @param position of the <code>Point</code> in the 2D Grid
	 * @param facing the direction of facing
	 * @see Point
	 * @see Facing
	 */
	public Pose(Point position, Facing facing) {
		this.position = position;
		direction = facing;
	}
	
	/**
	 * Get the current position.
	 *
	 * @return the value of position as a Point
	 */
	public Point getPosition() {
		return position;
	}
	
	/**
	 * Get the current direction of facing.
	 *
	 * @return the current direction as Facing
	 */
	public Facing getAngularPosition() {
		return direction;
	}
	
	/**
	 * Set the current direction of facing.
	 *
	 * @param direction the new direction as Facing
	 */
	public void setAngularPosition(Facing direction) {
		this.direction = direction;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + "[x=" + position.getX() + ",y=" + position.getY() + ",Facing=" + direction.toString() + "]";
	}

}
