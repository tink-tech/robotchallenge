package com.ioof.robotchallenge;


/**
 * This class represents a <code>Point</code> within a 2D Grid.
 * Increasing x values go from left to right, 
 * and increasing y values go from bottom to top.
 * 
 * Inspired by java.awt.Point, with some improvements
 *
 * @author Craig Tinkler (craig@tink-tech.com)
 * 
 */

public class Point {

	/**
	 * The x coordinate.
	 *
	 * @see #getX()
	 * @see #setLocation(int, int)
	 */
	private int x;

	/**
	 * The y coordinate.
	 *
	 * @see #getY()
	 * @see #setLocation(int, int)
	 */
	private int y;
	
	/**
	 * Initialises a new instance of <code>Point</code> representing the coordinates
	 * (0, 0)
	 */
	public Point() {
		x = 0;
		y = 0;
	}

	/**
	 * Initialises a new instance of <code>Point</code> with coordinates identical
	 * to the coordinates of the specified point.
	 *
	 * @param p the <code>Point</code> to copy the coordinates from
	 * @throws NullPointerException if p is null
	 */
	public Point(Point p) {
		x = p.getX();
		y = p.getY();
	}

	/**
	 * Initialises a new instance of <code>Point</code> with the specified
	 * coordinates.
	 *
	 * @param x the X coordinate
	 * @param y the Y coordinate
	 * @throws IllegalArgumentException if any coordinates are negative
	 **/
	public Point(int x, int y) { //throws IllegalArgumentException {
		//if (x < 0 || y < 0)
			//throw new IllegalArgumentException("Coordinates cannot be negative");
		
		this.x = x;
		this.y = y;
	}

	/**
	 * Get the x coordinate.
	 *
	 * @return the value of x, as an integer
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get the y coordinate.
	 *
	 * @return the value of y, as an integer
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets this object's coordinates to match those of the specified point.
	 *
	 * @param p the point to copy the coordinates from
	 * @throws NullPointerException if p is null
	 */
	public void setLocation(Point p) {
		x = p.getX();
		y = p.getY();
	}

	/**
	 * Sets this object's coordinates to the specified values.
	 *
	 * @param x the new X coordinate
	 * @param y the new Y coordinate
	 */
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Changes the coordinates of this point such that the specified <code>dx</code>
	 * parameter is added to the existing X coordinate and <code>dy</code> is added
	 * to the existing Y coordinate.
	 *
	 * @param dx the amount to add to the X coordinate
	 * @param dy the amount to add to the Y coordinate
	 */
	public void translate(int dx, int dy) {
		x += dx;
		y += dy;
	}

	/**
	 * Tests whether or not this object is equal to the specified object. This will
	 * be true if and only if the specified object is an instance of Point and has
	 * the same X and Y coordinates.
	 *
	 * @param obj the object to test against for equality
	 * @return true if the specified object is equal
	 */
	@Override
	public boolean equals(Object obj) {
		// NOTE: No special hashCode() method is required for this class,
		// as this equals() implementation is functionally equivalent to
		// super.equals(), which does define a proper hashCode().

		if (!(obj instanceof Point)) {
			return false;
		}
		Point p = (Point) obj;
		return x == p.getX() && y == p.getY();
	}

	/**
	 * Returns a string representation of this object. The format is:
	 * <code>getClass().getName() + "[x=" + x + ",y=" + y + ']'</code>.
	 *
	 * @return a string representation of this object
	 */
	@Override
	public String toString() {
		return getClass().getName() + "[x=" + x + ",y=" + y + ']';
	}

}