package com.ioof.robotchallenge;

/**
 * This class represents a 2D grid with boundary check
 * 
 * @author Craig Tinkler (craig@tink-tech.com)
 * 
 */
public class Grid {
	
	/**
	 * The number of rows in the grid.
	 */
	private int rows;

	/**
	 * The number of columns in the grid.
	 */
	private int columns;

	/**
	 * Initialises a new instance of <code>Grid</code>
	 *
	 * @param rows Number of rows within the Grid
	 * @param columns Number of columns within the Grid
	 * @throws IllegalArgumentException if any parameters are negative
	 */
	public Grid(int rows, int columns) throws IllegalArgumentException{
		if (rows < 0 || columns < 0) {
			throw new IllegalArgumentException("Parameters cannot be negative");
		}
		
		this.rows = rows;
		this.columns = columns;
	}
	
	/**
	 * Get the Number of Rows within the <code>Grid</code>
	 * @returns rows as integer
	 */
	public int getRows()
	{
		return rows;
	}
	
	/**
	 * Get the number of columns within the <code>Grid</code>
	 * @returns columns as integer
	 */
	public int getColumns()
	{
		return columns;
	}
	
	/**
	 * Checks to see if the supplied co-ordinate is within the specified boundary of <code>Grid</code>
	 *
	 * @param pos Position within the <code>Grid</code>
	 * @param d Dimension to check, either Rows or Columns
	 * @returns true if the Point is within the <code>Grid</code>
	 */
	public boolean checkWithinBounds(int pos, Dimension d) {
		if (pos >=0) {
			if (d.equals(Dimension.ROW)) {
				if (pos < rows) {
					return true;
				}
			} else if (d.equals(Dimension.COLUMN)){
				if (pos < columns) {	//0 indexed
					return true;
				}
			}
		}
		
		return false;
	}

	public enum Dimension { ROW, COLUMN; }
}
