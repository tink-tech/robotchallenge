package com.ioof.robotchallenge;


import com.ioof.robotchallenge.command.Commands;
import com.ioof.robotchallenge.command.Facing;

/**
 * This class represents a Robot than can be navigated within a 2D Grid.
 *
 * @see IRobot
 * @author Craig Tinkler (craig@tink-tech.com)
 */
public class Robot implements IRobot {

	/**
	 * 2D representation of x,y position and facing direction within a Grid.
	 */
	private Pose pose;
	
	/**
	 * The Grid of x Columns by y Rows that the robot can move within.
	 */
	private Grid table;
	
	
	/**
	 * Initialises a new instance of <code>Robot</code> and sets the <code>Grid</code> within which it will operate
	 *
	 * @param p the <code>Pose</code> of the Robot
	 * @param g the 2D <code>Grid</code> which the Robot will move within
	 * @see Pose
	 * @see Grid
	 */
    public Robot(Grid g) {
    	this.table = g;
    }
    
    /**
	 * Move the <code>Robot</code> forward one grid-space in the direction of facing.
	 * All invalid move requests, where the <code>Robot</code> would move outside the bounds
	 * of the grid are ignored.
	 */
    public void move() {
    	if (pose == null) {	//robot has not been placed into the grid
    		return;
    	}
    	
    	if (pose.getAngularPosition().equals(Facing.NORTH)) {
    		if (table.checkWithinBounds(pose.getPosition().getY() + 1, Grid.Dimension.ROW)) {
    			pose.getPosition().translate(0, 1);
    		}
    	} else if (pose.getAngularPosition().equals(Facing.SOUTH)) {
    		if (table.checkWithinBounds(pose.getPosition().getY() - 1, Grid.Dimension.ROW)) {
    			pose.getPosition().translate(0, -1);
    		}
    	} else if (pose.getAngularPosition().equals(Facing.EAST)) {
    		if (table.checkWithinBounds(pose.getPosition().getX() + 1, Grid.Dimension.COLUMN)) {
    			pose.getPosition().translate(1, 0);
    		}
    	} else if (pose.getAngularPosition().equals(Facing.WEST)) {
    		if (table.checkWithinBounds(pose.getPosition().getX() - 1, Grid.Dimension.COLUMN)) {
    			pose.getPosition().translate(-1, 0);
    		}
    	}
    }
    
    /**
	 * Rotate the <code>Robot</code> left or right 90 degrees.
	 * @param rotateCommand Direction of rotation
	 * @see Commands
	 */
    public void rotate(Commands rotateCommand) {    
    	if (pose == null) {	//robot has not been placed into the grid
    		return;
    	}
    	
    	if (rotateCommand.equals(Commands.LEFT)) {
    		if (pose.getAngularPosition().equals(Facing.NORTH)) {
    			pose.setAngularPosition(Facing.WEST);
    		} else if (pose.getAngularPosition().equals(Facing.WEST)) {
    			pose.setAngularPosition(Facing.SOUTH);
    		} else if (pose.getAngularPosition().equals(Facing.SOUTH)) {
    			pose.setAngularPosition(Facing.EAST);
    		} else if (pose.getAngularPosition().equals(Facing.EAST)) {
    			pose.setAngularPosition(Facing.NORTH);
    		}
    		
    	} else if (rotateCommand.equals(Commands.RIGHT)) {
    		if (pose.getAngularPosition().equals(Facing.NORTH)) {
    			pose.setAngularPosition(Facing.EAST);
    		} else if (pose.getAngularPosition().equals(Facing.EAST)) {
    			pose.setAngularPosition(Facing.SOUTH);
    		} else if (pose.getAngularPosition().equals(Facing.SOUTH)) {
    			pose.setAngularPosition(Facing.WEST);
    		} else if (pose.getAngularPosition().equals(Facing.WEST)) {
    			pose.setAngularPosition(Facing.NORTH);
    		}
    	}
    	
    }
    
    /**
	 * Place the <code>Robot</code> at the specified grid co-ordinates and direction of facing.
	 * All invalid <code>place</place> requests, where the <code>Robot</code> would move outside the bounds
	 * of the grid are ignored.
	 * @param pose Represents the 2D grid co-ordinate and facing direction
	 * @see Pose
	 */
    public void place(Pose pose) {
    	if ( table.checkWithinBounds(pose.getPosition().getX(), Grid.Dimension.COLUMN) 
    			&& table.checkWithinBounds(pose.getPosition().getY(), Grid.Dimension.ROW) ) {
    		if (this.pose == null) {	//robot has not yet been placed into grid
//    			if ( pose.getPosition().getX() == 0 && pose.getPosition().getY() == 0
//    					&& pose.getAngularPosition().equals(Facing.NORTH) ) {	
//    				//valid starting position of [0,0 NORTH]
    				this.pose = pose;
//    			}
    		} else {
    			this.pose.getPosition().setLocation(pose.getPosition());
        		this.pose.setAngularPosition(pose.getAngularPosition());
    		}
    		
    	}
    }
    
    /**
   	 * Get the current X,Y position and direction of facing as a <code>Pose</code>
   	 * @return pose
   	 * @see Pose
   	 */
    public Pose report() {
    	if (pose == null) {	//robot has not been placed into the grid
    		return null;
    	}
    	
    	return pose;
    }




}