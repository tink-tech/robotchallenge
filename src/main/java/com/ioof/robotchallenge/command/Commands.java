package com.ioof.robotchallenge.command;

/**
* This enum represents valid commands that can be issued to a <code>Robot</code>
* 
* @author Craig Tinkler (craig@tink-tech.com)
*/
public enum Commands {

	PLACE, MOVE, LEFT, RIGHT, REPORT, QUIT;
	
}
