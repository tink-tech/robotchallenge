package com.ioof.robotchallenge.command;

/**
* This enum represents direction within a Grid
* NORTH (up)
* SOUTH (down)
* EAST (right)
* WEST (left)
* 
* @author Craig Tinkler (craig@tink-tech.com)
* 
*/
public enum Facing {

	NORTH, SOUTH, EAST, WEST;

	public String toString() {
		switch(this) {
		case NORTH: return "North";
		case SOUTH: return "South";
		case EAST: return "East";
		case WEST: return "West";
		default:	return "unspecified";
		}
	}

}
