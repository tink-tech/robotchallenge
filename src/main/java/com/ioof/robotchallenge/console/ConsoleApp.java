package com.ioof.robotchallenge.console;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Hello world!
 *
 */
public class ConsoleApp 
{
    public static void main( String[] args )
    {
    	final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    	
    	CommandConsole console = new CommandConsole(reader, System.out);
    	
    	console.outputWelcomeMessage();
    	console.run();
    	console.outputGoodbyeMessage();
    }
    
}
