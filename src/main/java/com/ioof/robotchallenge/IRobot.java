package com.ioof.robotchallenge;

import com.ioof.robotchallenge.command.Commands;

/**
 * This interface represents a Robot than can be navigated within a 2D Grid.
 * 
 * @author Craig Tinkler (craig@tink-tech.com)
 */
public interface IRobot {

    void move();
    
	void rotate(Commands rotateCommands);
	
	void place(Pose pose);
	
	Pose report();
	
}
