package com.ioof.robotchallenge;

import com.ioof.robotchallenge.command.Commands;
import com.ioof.robotchallenge.command.Facing;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class RobotTest {

	@Test(expected=IllegalArgumentException.class)
	public void grid_throws_illegalargumentexception_with_negative_rows() {
		new Grid(-1, 0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void grid_throws_illegalargumentexception_with_negative_columns() {
		new Grid(0, -1);
	}
	
	@Test
	public void grid_withinBounds() {
		Grid grid = new Grid(10, 10);
		assertTrue(grid.checkWithinBounds(5, Grid.Dimension.COLUMN));
		assertTrue(grid.checkWithinBounds(5, Grid.Dimension.ROW));
	}
	
	@Test
	public void grid_outsideBounds() {
		Grid grid = new Grid(10, 10);
		assertFalse(grid.checkWithinBounds(10, Grid.Dimension.COLUMN)); //Grid Zero indexed (0-9) <<-- should fail
		assertFalse(grid.checkWithinBounds(10, Grid.Dimension.ROW));
		assertFalse(grid.checkWithinBounds(-1, Grid.Dimension.COLUMN));	//Any value less than zero should fail
		assertFalse(grid.checkWithinBounds(-1, Grid.Dimension.ROW));
	}

	@Test
	public void point_has_valid_movement() {
		Point p = new Point();			//Initialise a Point with default position of [0,0]
		assertEquals(p.getX(), 0);
		assertEquals(p.getY(), 0);
		
		p.translate(8, 14);
		assertEquals(p.getX(), 8);
		assertEquals(p.getY(), 14);
		
		p.translate(-3, 2);
		assertEquals(p.getX(), 5);
		assertEquals(p.getY(), 16);
		
		p.setLocation(7, 3);
		assertEquals(p.getX(), 7);
		assertEquals(p.getY(), 3);
		
		p.translate(8, -18);
		assertEquals(p.getX(), 15);
		assertEquals(p.getY(), -15);	
	}
	
	@Test
	public void point_setlocation_with_int() {
		Point p = new Point();			//Initialise a Point with default position of [0,0]
		
		p.setLocation(10, -20);
		assertEquals(p.getX(), 10);
		assertEquals(p.getY(), -20);
	}
	
	@Test
	public void point_setlocation_with_point() {
		Point p = new Point();			//Initialise a Point with default position of [0,0]
		
		assertEquals(p.getX(), 0);
		assertEquals(p.getY(), 0);
		
		Point newPoint = new Point(10,10);
		p.setLocation(newPoint);
		
		assertEquals(p.getX(), 10);
		assertEquals(p.getY(), 10);
		
	}
	
	@Test
	public void point_equality() {
		Point p = new Point(7,5);			//Initialise a Point with default position of [0,0]
		Point otherPoint = new Point(7,5);
		assertTrue(p.equals(otherPoint));
		
		Point anotherPoint = new Point(5,4);
		assertFalse(p.equals(anotherPoint));
	}
	
	
	@Test
	public void robot_set_initial_pose_to_0_0_NORTH_success() {
		Robot robby = new Robot(new Grid(10,10));
		robby.place(new Pose(new Point(0,0), Facing.NORTH));
		
		assertEquals(robby.report().getPosition().getX(), 0);
		assertEquals(robby.report().getPosition().getY(), 0);
		assertEquals(robby.report().getAngularPosition(), Facing.NORTH);
	}
	
	
	@Test
	public void robot_cannot_move_out_of_bounds() {
		Robot robby = new Robot(new Grid(5,5));
		robby.place(new Pose(new Point(0,0), Facing.NORTH));	//valid initial place
		//Position[0,0,NORTH]
		
		robby.move();	//Position[0,1]
		robby.move();	//Position[0,2]
		robby.move();	//Position[0,3]
		robby.move();	//Position[0,4]
		robby.move();	//Position[0,5]	** Out of bounds, should be ignored **
		
		assertEquals(robby.report().getPosition().getX(), 0);
		assertEquals(robby.report().getPosition().getY(), 4);
		
		robby.rotate(Commands.RIGHT);
		
		robby.move();	//Position[1,4]
		robby.move();	//Position[2,4]
		robby.move();	//Position[3,4]
		robby.move();	//Position[4,4]
		robby.move();	//Position[5,4]	** Out of bounds, should be ignored **
		
		assertEquals(robby.report().getPosition().getX(), 4);
		assertEquals(robby.report().getPosition().getY(), 4);
		
		robby.rotate(Commands.RIGHT);
		
		robby.move();	//Position[4,3]
		robby.move();	//Position[4,2]
		robby.move();	//Position[4,1]
		robby.move();	//Position[4,0]
		robby.move();	//Position[4,-1]	** Out of bounds, should be ignored **
		
		assertEquals(robby.report().getPosition().getX(), 4);
		assertEquals(robby.report().getPosition().getY(), 0);
		
		robby.rotate(Commands.RIGHT);
		
		robby.move();	//Position[3,0]
		robby.move();	//Position[2,0]
		robby.move();	//Position[1,0]
		robby.move();	//Position[0,0]
		robby.move();	//Position[-1,0]	** Out of bounds, should be ignored **
		
		assertEquals(robby.report().getPosition().getX(), 0);
		assertEquals(robby.report().getPosition().getY(), 0);
	}
	
	@Test
	public void robot_can_be_placed_within_grid() {
		Robot robby = new Robot(new Grid(5,5));
		robby.place(new Pose(new Point(0,0), Facing.NORTH));	//valid initial place
		//Position[0,0,NORTH]
		
		robby.place(new Pose(new Point(3,3), Facing.NORTH));
		
		assertEquals(robby.report().getPosition().getX(), 3);
		assertEquals(robby.report().getPosition().getY(), 3);
		assertEquals(robby.report().getAngularPosition(), Facing.NORTH);
	}
	
	@Test
	public void robot_cannot_be_placed_outside_grid() {
		Robot robby = new Robot(new Grid(5,5));
		robby.place(new Pose(new Point(0,0), Facing.NORTH));	//valid initial place
		//Position[0,0,NORTH]
		
		robby.place(new Pose(new Point(6,3), Facing.NORTH));	//Ignored, out of bounds
		
		assertEquals(robby.report().getPosition().getX(), 0);
		assertEquals(robby.report().getPosition().getY(), 0);
		assertEquals(robby.report().getAngularPosition(), Facing.NORTH);
	}
	
	@Test
	public void robot_can_turn_left_and_right() {
		Robot robby = new Robot(new Grid(5,5));
		robby.place(new Pose(new Point(0,0), Facing.NORTH));	//valid initial place
		//Position[0,0,NORTH]
		
		robby.rotate(Commands.RIGHT);
		robby.rotate(Commands.RIGHT);
		robby.rotate(Commands.RIGHT);
		robby.rotate(Commands.RIGHT);
		robby.rotate(Commands.RIGHT);
		
		assertEquals(robby.report().getAngularPosition(), Facing.EAST);
		
		robby.rotate(Commands.LEFT);
		robby.rotate(Commands.LEFT);
		robby.rotate(Commands.LEFT);
		
		assertEquals(robby.report().getAngularPosition(), Facing.SOUTH);
	}
	
	
}
